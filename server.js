const express = require('express')
const Sequelize = require('sequelize')
const _USERS = require('./users.json')
const bodyParser = require('body-parser')
const Op = Sequelize.Op

const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

const connection = new Sequelize('db', 'user', 'pass', {
    host: 'localhost',
    dialect: 'sqlite',
    storage: 'db.sqlite', operatorsAliases: false,
    define: {
        // freezeTableName: true // no need to pluralize the table name
    }
})

const User = connection.define('User', {
    id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
    },
    first: {
        type: Sequelize.STRING,
        validate: {
            len: [3, 10], // min and max
        }
    },
    last: Sequelize.STRING,
    full_name: Sequelize.STRING,
    email: {
        type: Sequelize.STRING,
        validate: {
            isEmail: true
        }
    },
    password: {
        type: Sequelize.STRING,
        validate: {
            isAlphanumeric: true
        }
    },
}, {
    timestamps: true, // createdAt and updatedAt
    hooks: {
        beforeValidate: () => {
            console.log('Before validate')
        },
        afterValidate: () => {
            console.log('After validate')
        },
        beforeCreate: (user) => {
            user.full_name = `${user.first} ${user.last}`
            console.log('Before create')
        },
        afterCreate: () => {
            console.log('After create')
        }
    }
})

const Post = connection.define('Post', {
    // id: {
    //     primaryKey: true,
    //     type: Sequelize.UUID,
    //     defaultValue: Sequelize.UUIDV4
    // },
    title: Sequelize.STRING,
    content: Sequelize.TEXT
})

const Comment = connection.define('Comment', {
    the_comment: Sequelize.STRING
})

const Project = connection.define('project', {
    title: Sequelize.STRING
})

Post.belongsTo(User, {
    as: 'UserRef', // Alias
    foreignKey: 'userId', // custom foreign key name
}) // puts foreignkey UserId in Post table

// One-to_many
Post.hasMany(Comment, {
    as: 'all_comments' // foreign key of PostId
})

// Many-to-Many
// Creates a UserProjects table with IDs for ProjectId and UserId
User.belongsToMany(Project, { as: 'Tasks', through: 'UserProject'})
Project.belongsToMany(User, { as: 'Workers', through: 'UserProject'})

connection
.sync({
    // logging: console.log,
    force: true // drop and create new
})
.then(() => {
    User.bulkCreate(_USERS)
    .then((users) => {
        console.log('Success adding users')
        Project.create({
            title: 'Project 1'
        })
        .then(project => {
            project.setWorkers([users[0].id, users[1].id])
        })
    })
    .then(() => {
        Project.create({
            title: 'Project 2'
        })
    })
    .catch(e => {
        console.log(e)
    })
})
.then(() => {
    console.log('success')
})
.catch(e => {
    console.log(e.message)
})


app.post('/user', (req, res) => {
    const newUser = req.body.user
    User.create({
        first: newUser.first,
        last: newUser.last,
        email: newUser.email,
        password: newUser.password
    })
    .then(user => {
        res.json(user)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.get('/', (req, res) => {
    User.create({
        first: 'Jane',
        last: 'Doe',
        bio: 'New bio entry 2 foo'
    })
    .then(user => {
        res.json(user)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.post('/find_all', (req, res) => {
    User.findAll({
        where: {
            first: req.body.first
        }
    })
    .then(user => {
        res.json(user)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.post('/find_all_first', (req, res) => {
    User.findAll({
        where: {
            first: {
                [Op.like]: `${req.body.first}%`
            }
        }
    })
    .then(user => {
        res.json(user)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.post('/find_one', (req, res) => {
    User.findById(req.body.id)
    .then(user => {
        res.json(user)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.delete('/user', (req, res) => {
    User.destroy({
        where: {
            id: req.body.id
        }
    })
    .then(() => {
        res.status(202).json({"message": `user with id ${req.body.id} was successfully deleted`})
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.put('/user', (req, res) => {
    User.update({first: req.body.first}, {where: {id: req.body.id}})
    .then(rows => { // only return effected row
        if(rows > 0) {
            User.findById(req.body.id)
            .then(user => {
                res.json(user)
            })
        } else {
            res.status(404).json({"error": "No user found"})
        }
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.post('/posts', (req, res) => {
    Post.create({
        userId: req.body.id,
        title: req.body.title,
        content: req.body.content
    })
    .then(post => {
        res.json(post)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.get('/posts', (req, res) => {
    Post.findAll({
        include: [{
            model: User,
            as: 'UserRef'
        }]
    })
    .then(posts => {
        res.json(posts)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.post('/comments', (req, res) => {
    Comment.create({
        PostId: req.body.id,
        the_comment: req.body.comment,
    })
    .then(comment => {
        res.json(comment)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.get('/single_post', (req, res) => {
    Post.findById(req.query.id, {
        include: [{
            model: Comment,
            as: 'all_comments',
            attributes: ['the_comment']
        },
        {
            model: User,
            as: 'UserRef'
        }]
    })
    .then(posts => {
        res.json(posts)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.put('/add_worker', (req, res) => {
    Project.findById(req.body.id)
    .then(project => {
        project.addWorkers(req.body.user_id)
    })
    .then(() => {
        res.status(202).json({'message': 'User Added'})
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

app.get('/get_user_project', (req, res) => {
    User.findById(req.query.id, {
        attributes: ['first', 'last', 'id'],
        include: [{
            model: Project, as: 'Tasks',
            attributes: ['title']
        }]
    })
    .then(projects => {
        res.json(projects)
    })
    .catch(e => {
        console.log(e)
        res.status(404).send(e)
    })
})

const port = 8001
app.listen(port, () => {
    console.log('Running server on port ' + port)
})